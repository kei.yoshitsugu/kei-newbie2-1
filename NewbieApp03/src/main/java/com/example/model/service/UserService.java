package com.example.model.service;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.model.dto.User;
import com.example.model.repository.UserRepository;

@Service
public class UserService {
  @Autowired
  UserRepository userRepository;

  @Autowired
  HttpSession session;

  public String getUserPassword(String email) {
    return userRepository.getUserPassword(email);
  }

  public void signup(User user) {
    userRepository.signup(user);
  }
}
