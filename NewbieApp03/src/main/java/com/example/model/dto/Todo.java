package com.example.model.dto;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Todo {
  private String title;
  private Integer status;
  private String user_email;
  private Integer id;
  private Date created_by;
}
