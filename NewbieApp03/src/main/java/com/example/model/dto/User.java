package com.example.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
  private String email;
  private String password;
}
