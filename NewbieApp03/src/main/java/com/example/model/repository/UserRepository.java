package com.example.model.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.example.model.dto.User;

@Repository
@Mapper
public interface UserRepository {
  String getUserPassword(String email);

  void signup(User user);
}
