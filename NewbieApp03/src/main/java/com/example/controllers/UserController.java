package com.example.controllers;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.model.dto.User;
import com.example.model.service.UserService;

@RestController
@CrossOrigin
public class UserController {
  @Autowired
  UserService userService;


  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String login(HttpServletRequest request, HttpServletResponse response,
      @ModelAttribute User json) throws AuthenticationException {
    String email = json.getEmail();
    String password = json.getPassword();
    boolean valid = password.equals(userService.getUserPassword(email));
    if (valid) {
      HttpSession session = request.getSession(true);
      System.out.println(session.getId());
      session.setAttribute("user", json);
      response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:8887");
      response.setHeader("Access-Control-Allow-Credentials", "true");
      return session.getId();
    } else {
      throw new AuthenticationException();
    }
  }

  @RequestMapping(value = "/logout")
  public void logout(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession();
    session.removeAttribute("user");
    response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:8887");
    response.setHeader("Access-Control-Allow-Credentials", "true");
  }

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  public void signup(@ModelAttribute User json) {
    userService.signup(json);
  }
}
