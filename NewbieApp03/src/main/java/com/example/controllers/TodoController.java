package com.example.controllers;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.model.service.UserService;

@RestController
public class TodoController {
  @Autowired
  UserService userService;

  @CrossOrigin
  @RequestMapping(value = "/todo", method = RequestMethod.GET)
  public void get_index(HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException {
    HttpSession session = request.getSession();
    response.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:8887");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    if (session.getAttribute("user") != null) {
      // JSONを返す処理
    } else {
      throw new AuthenticationException();
    }

  }
}
