package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewbieApp03Application {

	public static void main(String[] args) {
		SpringApplication.run(NewbieApp03Application.class, args);
	}

}
